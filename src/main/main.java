/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import nomer1.Kost;
import nomer1.Pemilik;
import nomer1.Penyewa;

/**
 *
 * @author liuslangobelen
 */
public class main {
    public static void main(String[] args) {
        Kost r = new Kost("Raflesia","Sukapura",5);
        Pemilik y = new Pemilik("Pa Yayat","08222111889");
        Penyewa p1 = new Penyewa("Thanos","+62827989999");
        Penyewa p2 = new Penyewa("Ant Man","+62827989999");
        Penyewa p3 = new Penyewa("Iron Man","+62827989999");
        Penyewa p4 = new Penyewa("Bat Man","+62827989999");
        Penyewa p5 = new Penyewa("Super Man","+62827989999");
        r.setPemilik(y);
        r.addPenyewa(p1);
        r.addPenyewa(p2);
        r.addPenyewa(p3);
        r.addPenyewa(p4);
        r.addPenyewa(p5);
        
        System.out.println("-Nama Kost    :"+r.getNama());
        System.out.println("-Alamat Kost  :"+r.getAlamat());
        System.out.println("-Kode Kost    :"+r.getNama());
        System.out.println("--Nama Pemilik  :"+r.getPemilik().getNama());
        System.out.println("--No.HP Pemilik  :"+r.getPemilik().getNohp());
        System.out.println("---Kelima nama Penyewa Pada Kost"+r.getNama()+"Adalah :");
        for (int i = 0; i < r.getPenyewa().length; i++) {
            System.out.print("---Penyewa Ke-");System.out.println(i+1);
            System.out.println("----Nama    :"+r.getPenyewa()[i].getNama());
            System.out.println("----No.HP   :"+r.getPenyewa()[i].getNohp());
        }
    }
}
